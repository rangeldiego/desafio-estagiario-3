# desafio-estagiario-3

### Desafio RPE de Seleção 
Olá, queremos convidá-lo a participar de nosso desafio de seleção.  Pronto para participar? Seu trabalho será visto por nosso time e você receberá ao final um feedback sobre o que achamos do seu trabalho. Não é legal?

### Sobre a oportunidade 
A vaga é para Estágio em Desenvolvimento Java.

### Desafio Técnico

  - Resumo do problema: Hoje na empresa estamos com muitos veículos de diferentes portes, e todo esse controle de cadastro está sendo através de papel físico. Com isso precisamos de uma sistema para manter o cadastro de todos esses veículos.
  
  
  - Dicionário:
    ```
    * Veículo de Passeio: Veículo utilizado para alguns funcionários visitarem seus clientes.
      Atributos: Placa, Nome, Marca, Número de Passageiros
        
    * Veículo de Carga: Veículo utilizado para fazer entregas de mercadorias.
      Atributos: Placa, Nome, Marca, Capacidade, Quantidade de Carroceria
    ```

  Objetivo do Desafio: Desenvolver uma api que tenha uma função de CRUD para manter os cadastros de veículos.
  
    
  - Pré-requisitos:
    ```
    * Utilização de banco de dados Oracle, MySQL, H2, Postgres ou qualquer outro banco relacional.
    * Java 17+
    * Maven
    * Swagger

    ```

  - O que esperamos como escopo:
    ```
    * Endpoint para adicionar um Veículo de Passeio
    * Endpoint para adicionar um Veículo de Carga
    * Endpoint para consultar um Veículo de Passeio
    * Endpoint para consultar um Veículo de Carga
    * Endpoint para alterar um Veículo de Passeio
    * Endpoint para alterar um Veículo de Carga
    * Endpoint para remover um Veículo de Passeio
    * Endpoint para remover um Veículo de Carga

    ```

  - Não precisa desenvolver:
    ```
    * Front
    ```

  - Extra/Bônus (Não Obrigatório)
    ```
    * Implementar testes unitários
    ```
  
  - O que vamos avaliar:
    ```
    * Organização de código;
    * Funcionamento;
    * Boas práticas;
    ```

### Instruções
        1. Crie um projeto no gitlab ou github;
        2. Desenvolva. Você terá 5 (cinco) dias a partir da data do envio do desafio; 
        3. Crie um arquivo de texto com a nomenclatura README.md com a explicação de como devemos executar o 
        projeto e com uma descrição do que foi feito; 
        4. Envie o link do projeto respondendo ao email enviado.

